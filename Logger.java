import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

class Logger{
	  String FILEPATH="/home/nick/assess/CSE/";

  Logger()
  {
    System.out.println("Initializing logger");
  }

  synchronized void log(String msg) // write grade to running logfile
  {
    try{
      PrintWriter logWriter=new PrintWriter(new BufferedWriter(new FileWriter(
                FILEPATH+"grades.txt",true))); // open/append
      logWriter.println(msg);
      logWriter.close();
    } catch (Exception e) {
      System.out.println("Gradefile write error: " + e);
    }
    System.out.println("Logger saved " + msg);
  }
}
