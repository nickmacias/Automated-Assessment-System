/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

// class for holding test results
class TestResult{
  String clientOut,goodOut,diff;
  int score,maxScore;

  TestResult(String co, String go, String d, int sc, int maxSc)
  {
    clientOut=co;
    goodOut=go;
    diff=d;
    score=sc;
    maxScore=maxSc;
  }

  public String toString()
  {
    return(score + "/" + maxScore);
  }
}
