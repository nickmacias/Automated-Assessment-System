import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

public class MyRun {
// execute the command (with given arguments), pass in->stdin return stdout

  static public String execute(String[] command, String in)
  {
    try{
      Process pr = Runtime.getRuntime().exec(command);
      InputStreamReader isr=new InputStreamReader(pr.getInputStream());
      BufferedReader input = new BufferedReader(isr);
      OutputStream output = pr.getOutputStream();

      String ret="";
      String line;

      output.write(in.getBytes()); // send input to process' stdin
      output.close(); // flush and close

      pr.waitFor(); // wait for process to finish

// read stdout from program
      while((line=input.readLine()) != null) {
        ret=ret+line+"\n";
      }

      return(ret);
    } catch (Throwable t){
      System.out.println("Execute error (test program terminated early?)");
      t.printStackTrace();
      return("");
    }

/*
 * Weirdness here:
 * if the program exits without reading any input, then execute()
 * will try writing to a closed channel, and will thrown an
 * exception "java.io.IOException: Broken pipe"
 *
 * But the second time you do this, it doesn't throw the exception...
 * Or, if you first run the program without feeding input, and then
 * re-run with input, no exception is thrown.
 *
 * NEW INSIGHT:
 * If the command has a small delay in it before it exit,
 * we don't see the broken pipe exception.
 * This suggests that it's not writing itself that causes the error,
 * but rather writing after the process has terminated.
 * So it may be that running the program once causes a delay
 * in subsequent executions (resetting the environment or something?)
 * which prevents the exception...
 *
 */
  }

}
