import java.net.ServerSocket;
import java.net.Socket;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

public class Server{

// Main class for code tester
// Allow commands to be executed, passing input and savong output
// Also, start a server to receive connection requests, and
// start a thread for each processed request.

  public static void main(String[] args) {
    if (args.length != 1){
      System.out.println("Usage: java Server ODPname");
      return;
    }

    try{
      ServerSocket listener=new ServerSocket(1209); // accept requests here
      Logger logger=new Logger();
      while (true){
        Socket socket=listener.accept();
        // start a thread to process this connection...
        SocketThread st=new SocketThread(socket);
        st.setODP(args[0]); // save ODP name
        st.saveLogger(logger);
        st.start();
      }
    } catch(Exception e){
      System.out.println("Socket error: " + e);
    }
  }

}
