aPath=/home/nick/assess

# extract source code from assess logfiles
alias extract="awk '\$0~/1======/,\$0~/0======/ {print FILENAME \":\" \$0}'"

# start/checkstop the server
alias server="$aPath/server"

# run the grader
alias grader="pushd $aPath;java Grader;popd"
