all: Server.class FileIO.class MyRun.class SocketThread.class TestCase.class \
      TestResult.class Logger.class Assess.class \
      assess.jar Grader.class
	
Server.class: Server.java
	javac Server.java
FileIO.class: FileIO.java
	javac FileIO.java
MyRun.class: MyRun.java
	javac MyRun.java
SocketThread.class: SocketThread.java
	javac SocketThread.java
TestCase.class: TestCase.java
	javac TestCase.java
TestResult.class: TestResult.java
	javac TestResult.java
Logger.class: Logger.java
	javac Logger.java
Assess.class: Assess.java
	javac Assess.java
assess.jar: FileIO.class MyRun.class SocketThread.class TestCase.class \
            TestResult.class Logger.class Assess.class AssessExe.class
	jar cvf assess.jar FileIO.class MyRun.class SocketThread.class \
                TestCase.class TestResult.class Logger.class Assess.class \
                AssessExe.class
Grader.class: Grader.java
	javac Grader.java
clean:
	rm Server.class FileIO.class MyRun.class SocketThread.class \
           TestCase.class TestResult.class Logger.class Assess.class \
           assess.jar
