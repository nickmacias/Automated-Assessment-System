import java.net.Socket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */


// Main class for test system (client side)
public class Assess{

  public static void main(String[] args) {
    AssessExe ae=new AssessExe(args);
  }
}

class AssessExe
{
  AssessExe(String[] args)
  {

    String prompt="\nYou may choose from the following options:\n\n"+
                  //" ?         see a list of available test files\n"+
                  //" t name    test your code on file \"name\"\n"+
                  //" T name    test your code with verbose output\n"+
                  //" r         test your code on a randomly-selected test file\n"+
                  //" A         test your code on *all* test files\n"+
                  " G         submit your code for grading\n"+
                  " x         exit the assessment system\n"+
                  " h         for help\n\n"+
                  "Note that you may submit your assignment for grading more than once,\n"+
                  "in which case the latest submission will be counted.\n\n";

    String userProgName=null;
    String sourceProgName=null;

    if (args.length!=2){
      System.out.println("Usage: assess executable-name source-code");
      return;
    }
    userProgName=args[0]; // program to test
    sourceProgName=args[1]; // source code - save to logfile

    try{
      Socket  mainSock=new Socket("localhost",1209); // make connection request
      String uid=System.getProperty("user.name");
      System.out.println("Connected to server, userID=" + uid);

// setup I/O objects
      PrintWriter pw=new PrintWriter(mainSock.getOutputStream(),true); // for sending to socket
      BufferedReader in=new BufferedReader(new InputStreamReader(mainSock.getInputStream()));

// sync w/userID
      pw.println(uid);pw.flush();

// send source code followed by <<<EOT>>>
      sendSource(sourceProgName,pw);

      String response=in.readLine();
      if (!response.equals("OK")){
        System.out.println(
            "ERROR: Your userid (" + uid +
            ") is not recognized by the server.\n"+
            "Contact your instructor to have this remedied.");
        return;
      }

      System.out.println("Welcome to the Automated CSE Assessment System.");
      System.out.println("\n"+prompt+"\n");
      System.out.println("***************************************************");
      System.out.println("You are about to assess your code for:\n<" +
                          in.readLine() + ">\n");
      System.out.println("***************************************************");
      Scanner sc=new Scanner(System.in);
      System.out.print("Command> ");

      boolean verbose;
      boolean graded=false;
// main loop
      while (sc.hasNextLine()){
        verbose=false;
        String cmd=sc.nextLine();
        if (cmd.length()==0) cmd=" ";
        switch(cmd.charAt(0)){

          case 'x': // exit
            pw.println("x");pw.flush();
            mainSock.close();
            if (!graded){
              System.out.println("\n\n**********************\n"+
                 "WARNING: You haven't submitted your assignment for grading yet.\nDon't forget to use the 'G' command.\n**********************\n");
            }
            System.out.println("User exit requested - goodbye");
            return;

/***
          case '?': // get list of files
            pw.println(cmd);
            pw.flush();
            System.out.println(in.readLine());
            in.readLine(); // flush <EOT>
            break;

          case 'T': verbose=true;
          case 't': // t name - test file "name"
          case 'r': // random test
            pw.println(cmd);
            pw.flush();
            if (!singleTest(userProgName,in,pw,verbose)){
              return; // unexpected failure
            }
            break;

         case 'A': // run all tests
            pw.println(cmd);
            pw.flush();
            while (singleTest(userProgName,in,pw,false)); // run all tests
            break;
***/

          case 'G': // run grading tests
            pw.println(cmd);
            pw.flush();
            while (singleTest(userProgName,in,pw,false)); // run all tests
            graded=true;
            break;

          default: // unknown
            System.out.println("Unknown command: " + cmd);
            System.out.println(prompt);
        }
// end of loop
        System.out.print("Command> ");
      }
      System.out.println("User exit requested - goodbye");
      pw.println("x");pw.flush();
      mainSock.close();
      return;
    } catch(Exception e){
      System.out.println("Socket error: " + e);
    }
  }

// send source code to server
  void sendSource(String sourceProgName,PrintWriter pw)
  {
    pw.println("src=<" + sourceProgName + ">");
    Scanner sc=null;
    try{
      sc=new Scanner(new File(sourceProgName));
    } catch (Exception e){
      System.out.println("Error reading " + sourceProgName + ": " + e);
      pw.close();
      System.exit(1);
    }

    while (sc.hasNextLine()){
      pw.println(sc.nextLine());
    }
    sc.close();
    pw.println("<<<EOT>>>"); // signal completion of source code transmission
    pw.flush();
  }
 

// run a single test; return true if it succeeds, false if empty test
  boolean singleTest(String userProgName,
                            BufferedReader in, PrintWriter pw,
                            boolean verbose)
  {
    try{
// read until <EOT>
      String stdin="";
      String temp;

// we should receive an argument list, followed by input lines for stdin
      String testArgs=in.readLine(); // pass these arguments to program

// but if we're done with tests ('A'), we should receive <EOT>
      if (testArgs.equals("<EOT>")) return(false); // end of tests

      while(!(temp=in.readLine()).equals("<EOT>")){
        stdin=stdin+temp+"\n";
      } // stdin is a String version of the test input

      if (verbose){
        System.out.println("Running program with arguments <" +
                           testArgs + ">");
        System.out.println("and stdin=\n---------------------\n"+
                                 stdin+"---------------------");
      }

// run user's program with testArgs, and feed stdin to it
      String[] c=(userProgName + " " + testArgs).split(" ");
      String progOut=MyRun.execute(c,stdin); // run user's program

// send output to server with <EOT> at end
      pw.println(progOut+"<EOT>");pw.flush();

// then read results from server and show to user
      String stdout="";
      while(!(temp=in.readLine()).equals("<EOT>")){
        stdout=stdout+temp+"\n";
      }

// show test results
      System.out.println("Results:\n"+stdout);
      return(true); // good test
    } catch (Exception e){
      System.out.println("Unexpected error (bad command?) <" + e + ">");
      return(false);
    }
  }

}
