/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

public class TestCase {
  String args;  // space-separated list of arguments to program (or "")
  String inputFile; // Name of testfile
  boolean grade; // true means this is a graded test
  TestCase(String a, String i, Boolean g)
  {
    args=a;inputFile=i;grade=g;
  }
}
