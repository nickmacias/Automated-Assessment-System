import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;
import java.util.Hashtable;
import java.time.LocalDateTime;

/*
 * Automated Assesment System - Grade Recorder
 *
 * read grades.txt, save all data
 * read sid.txt, save uid/sid
 * read Grade-template.csv, merge uid
 * for each project, get uid from Grade-template, add grade from grades.txt
 *   and print to .csv file.
 *
 */

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

class Grader{
  public static void main(String[] args)
  {
    String FILEPATH="./CSE/";

// Hashtable of all grades, keyed by assignment:sid
    Hashtable<String,Integer> grades=new Hashtable<String,Integer>();

// map from uid -> sid
    Hashtable<String,String> uidsid=new Hashtable<String,String>();
  
// Vector of all assignment
    Vector<String> allAssignments=new Vector<String>();

// Vector of grade template entries
    Vector<String> templates=new Vector<String>();

// read and process grades, merge SIDs; build assignment list too
    try{
      BufferedReader br;
      String input;
      String[] parts;

// read Grading-Template.csv, save in array
      br=new BufferedReader(new FileReader(FILEPATH+"Grading-Template.csv"));
      while ((input=br.readLine())!=null){
        templates.add(input);
      }
      br.close();

// read uid/sid, save in hash
      br=new BufferedReader(new FileReader(FILEPATH+"sid.txt"));
      while ((input=br.readLine())!=null){
        parts=input.split(",");
        uidsid.put(parts[0],parts[1]); // can get sid for a uid now
      }
      br.close();

// now read raw grade file
      br=new BufferedReader(new FileReader(FILEPATH+"grades.txt"));
      while ((input=br.readLine())!=null){ // parse the input
        parts=input.split(":");
        String ass=parts[0]; // assignment
        String uid=parts[1];
        String sid=uidsid.get(uid); // find student id
        if (sid==null){
          System.out.println("ERROR: UserID " + uid +
                             " not found in sid.txt");
          sid="000000000";
        }

// calculate grade
        int g=(int) Math.round(100.*Double.parseDouble(parts[5])/
                                    Double.parseDouble(parts[6]));

        String key=ass + ":" + sid; // key for this entry
        grades.put(key,g); // save this grade (overwrites prior entries)

// save assignment
        if (allAssignments.indexOf(ass)==-1){
          allAssignments.add(ass);
        }
      } // all grades loaded
      br.close();
    } catch (Exception e) {
      System.out.println("Error processing raw grade file: " + e);
      return;
    }

// Finally, read templates, append grades, and output to .csv

    try{
      PrintWriter pw=new PrintWriter(new BufferedWriter(new FileWriter(
                FILEPATH+"upload.csv")));
   
      for (int i=0;i<templates.size();i++){
        String prefix=templates.get(i); // beginning of line
        pw.print(prefix+",");
        for (int ass=0;ass<allAssignments.size();ass++){ // assignment #ass
          String comma=(ass==allAssignments.size()-1)?"":",";
          String assName=allAssignments.get(ass); // assignment name
          if (i==0){ // header
            pw.print(assName + comma);
          } else if (i==-1){ // possible points // skip this (never == -1)
            pw.print("100" + comma);
          } else {// print actual grade (if found)
            String sid=prefix.split(",")[2];
            Integer g=grades.get(assName + ":" + sid);
            if (g==null) g=0;
            pw.print(g + comma);
          }
        } // end of line
        pw.println();
      } // all students done
      pw.close();
    } catch(Exception e){
      System.out.println("Error writing output: " + e);
      return;
    }
  }
}
