import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;
import java.time.LocalDateTime;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

/*
 * Basic interaction:
 * Client: send request to server
 * Server: accept connection, start thread
 * Client: send UID followed by source code, followed by <<<EOT>>>
 * Server: save UID; lookup in table; send OK (good) or UU (unknown UID); quit if invalid UID
 *         otherwise, read source code until <<<EOT>>> and write to logfile. Server writes to logfile.
 * Client: send command (t=random test; ?=list test names; t name=test name; T=test all;G=grade;x=exit)
 * Server: respond to command. If starting a test:
 * Server: send args, send input
 * Client: run user program with given args and input. Send stdout to server
 * Server: save client's output to file. Run goodprogram with args and input, save stdout to file
 * Server: run difference on outputs; send report + grade->Client. Log results; If grading, save grade.
 * Client: display test results.
 */

// main class for running a separate thread to process one test request
public class SocketThread extends Thread{
  String FILEPATH="/home/nick/assess/CSE/";
  
  Socket theSocket; // main communication with client
  String goodProgramID=null;
  String programInfo=null;
  String ODP="xxxxxxxxxxx"; // set to e.g. ODP401
  String programID=null;
  String goodProgName=null;
  String userID; // actual (Canvas) username of user
  Vector<TestCase> testCases=new Vector<TestCase>();

  static Logger logger; // Thread-safe logging
  
// vars for logfile writing
  boolean needFileInit=true;
  PrintWriter logWriter=null;

  SocketThread(Socket s)
  {
    theSocket=s;
  }

  public void setODP(String ODP)
  {
    this.ODP=ODP;
  }
  
  public void run()
  {
    System.out.println("Processing config file from " +
                        FILEPATH+"/"+ODP+"/"+"config.txt");
// read config file
    readConfigFile(FILEPATH+"/"+ODP+"/"+"config.txt");

// Get I/O channels to client
    try{
      PrintWriter pw=new PrintWriter(theSocket.getOutputStream(),true); // for sending to socket
      BufferedReader in=new BufferedReader(new InputStreamReader(theSocket.getInputStream()));

// read UID, find actual username for grading (reject if not found)
      String UID=in.readLine();
      userID=findUID(UID);
      if (userID==null){ // invalid userid
        pw.println("UU"); // unknown userid
        pw.flush();
        pw.close(); // goodbye...
        theSocket.close();return; // ignore source code!
      }
      log("Connected on socket " + theSocket);
      log("UID=" + userID);

// read source code until <<<EOT>>>
      log("1=========================================================");
      String srcLine=in.readLine();
      while ((!srcLine.equals("<<<EOT>>>")) && (srcLine != null)){
        log(srcLine);
        srcLine=in.readLine();
      }
      log("0=========================================================");
      pw.println("OK\n"+programInfo);pw.flush();
      
      TestResult results;
// read command from client
      String command;
      while (null != (command=in.readLine())){
        log("Received command <" + command + ">");
        if (command.length()==0) command="x";
        switch(command.charAt(0)){
          case 'r': // random test
            int ind=(int) (Math.random()*(float)testCases.size());
            while (testCases.get(ind).grade){ // find an ungraded test case
              ind=(++ind)%testCases.size();
            }
            results=doTest(in,pw,ind);
            pw.print(results.score + "/" + results.maxScore + "\n"+
                       results.diff);
            pw.println("<EOT>");
            pw.flush();
            break;
          case 't': // t input-file-name
          case 'T': // verbose form (no change for us)
            if (command.length() < 2) command="t  "; // illegal
            ind=(-1);
            for (int i=0;i<testCases.size();i++){
              if (command.substring(2).equals(testCases.get(i).inputFile) &&
                  (!testCases.get(i).grade)){ // run this test
                ind=i;break;
              }
            }
            if (ind==(-1)){ // no such test case
              pw.println("No such testfile " + command.substring(2));
              pw.flush();theSocket.close();return;
            } // else run the command
            results=doTest(in,pw,ind);
            pw.print(results.score + "/" + results.maxScore + "\n"+
                       results.diff);
            pw.println("<EOT>");
            pw.flush();
            break;
          case 'A': // all tests
            for (int i=0;i<testCases.size();i++){
              if (!testCases.get(i).grade){
                results=doTest(in,pw,i);
                pw.println(results.score + "/" + results.maxScore + "\n"+
                           results.diff);
                pw.println("<EOT>");
                pw.flush();
              } // end of that test case
            }
            pw.println("<EOT>");pw.flush();
            break;
          case 'G': // run grading tests (later)
// prepare to tally all scores
            int totalPoints=0;
            int maxPoints=0;

            for (int i=0;i<testCases.size();i++){
              if (testCases.get(i).grade){
                results=doTest(in,pw,i);
                totalPoints+=results.score;
                maxPoints+=results.maxScore;
                pw.println(results.score + "/" + results.maxScore);
                pw.println("<EOT>");
                pw.flush();
              } // end of that test case
            }
            pw.println("<EOT>");pw.flush();
// send final score to server %%%
            logger.log(programID + ":" + userID + ":[" +
                       LocalDateTime.now() + "]:" + totalPoints +
                       ":" + maxPoints);
            break;
          case 'x': // exit
            log("Exiting");
            logWriter.close();
            theSocket.close();return;
          case '?': // list all available test cases
            for (int i=0;i<testCases.size();i++){
              if (!testCases.get(i).grade){
                pw.print(testCases.get(i).inputFile + " ");
              }
            }
            pw.println("\n<EOT>");pw.flush();
            break;
          default: // unknown command
            pw.println("Error: unknown command " + command);
            log("Error: unknown command " + command);
            pw.flush();theSocket.close();return;
        }
      }
     
// EOF from socket - goodbye!
      theSocket.close();
      log("Client exited?");
      return;
    } catch(Exception e){
      log("Socket error: " + e);
    }
  }
  
// main test code - send args and input file to client, read output, run goodprog, compare, log, report
  TestResult doTest(BufferedReader in ,PrintWriter pw, int index)
  {
    log("Running test " + index);
    String args=testCases.get(index).args;
    String inputFile=FILEPATH+programID+"/"+testCases.get(index).inputFile;

// send args and input to client
	pw.println(args);
	String input=FileIO.read(inputFile); // load test file into string
	pw.print(input);
        pw.println("<EOT>");pw.flush(); // no need for adding \n at end...

// load stdout from client
    String stdout=readStdout(in);
// run good program, compare to client's stdout
    TestResult compare=compareResults(stdout,args,inputFile);
    log("Client output:\n"+compare.clientOut);
    log("Server output:\n"+compare.goodOut);
    log("Differences:\n"+compare.diff);
    log("Score: " + compare.score + "/" + compare.maxScore);
    return(compare);
  }
  
// compare client's stdout with good program's stdout
  TestResult compareResults(String clientOut,
                        String args, String inputFile)
  {
// run good program (with arguments)
    String in=FileIO.read(inputFile); // to stdin
    String[] c=(goodProgName + " " + args).split(" "); // string array for cmd
    String serverOut=MyRun.execute(c,in); // read from stdout
// compare two outputs: write them to file
    String cFile=FILEPATH+"data/client."+theSocket.getPort();
    String sFile=FILEPATH+"data/server."+theSocket.getPort(); // output files

    FileIO.write(clientOut,cFile);
    FileIO.write(serverOut,sFile);

// now run scoreResults to get score and differences
    String diffs=MyRun.execute(
             (FILEPATH+"scoreResults " + cFile + " " + sFile).split(" "),
             "");

// remove temporary files
    new File(cFile).delete();
    new File(sFile).delete();

// find score and max-points
    String[] tmp=diffs.split("\n");
    int score=Integer.parseInt(tmp[0]);
    int maxScore=Integer.parseInt(tmp[1]);
    diffs="";
    for (int i=2;i<tmp.length;i++){
      diffs=diffs+tmp[i]+"\n"; // re-build string
    }

// create output
    TestResult tr=new TestResult(clientOut,serverOut,diffs,score,maxScore);
    return(tr);
  }

// read output from client until <EOT>
  String readStdout(BufferedReader in)
  {
    String res="";
    String temp;
    try{
      while (null != (temp=in.readLine())){
        if (temp.equals("<EOT>")) return(res);
        res=res+temp+"\n";
      }
      in.close(); // something went wrong
    } catch(Exception e){
      log("Client stdout read error: " + e);
    }
// Client exited?
    return(null);
  }

  /* Configuration file for one assignment
   * 
   *    ID of this task (subdir containing inputs and goodprog)
   *    Long description of this task
   *    Path to goodprog (program that runs correctly)
   *    testfile1
   *      arguments1
   *    G*testfile2      "G*" in front means graded testcase
   *      arguments2
   *    ...
   *    <EOF>
   */
  void readConfigFile(String fname) // load configuration information
  {
    try{
      BufferedReader br=new BufferedReader(new FileReader(fname));
      String args,input;
      programID=br.readLine(); // Usually an integer
      programInfo=br.readLine(); // save info about this test environment
      goodProgName=FILEPATH+programID+"/"+br.readLine(); // name of program we'll use to determine correct output
      while ((input=br.readLine())!=null){
        args=br.readLine(); // read inputfile/args pair

//if filename starts with G* then this is a graded testfile
        boolean grade;
        if (input.length() < 2){
          grade=false;
        } else {
          grade=input.substring(0,2).equals("G*");
        }
        if (grade) input=input.substring(2); // remove G*
        TestCase tc=new TestCase(args,input,grade);
        testCases.add(tc); // save to vector of testcases
      }
      br.close();
    } catch (Exception e) {
      log("File error: " + e);
    }
  }
  
  // check for user ID, return if found (null if not)
  String findUID(String uidIn)
  {
    return(uidIn); // skip the lookup -- accept all user IDs, convert in grader
/***
    try{
      BufferedReader br=new BufferedReader(new FileReader(FILEPATH+"users.txt"));
      String uid,name,text;
      while ((text=br.readLine())!=null){
        uid=text.substring(0, text.indexOf(" "));
        name=text.substring(1+text.indexOf(" "));
        if (uidIn.equals(uid)){ // found it
          br.close(); // done
          return(name);
        }
      }
      System.out.println("User " + uidIn +
                         " not found in config file - goodbye.");
      br.close();
      return(null); // didn't find it
    } catch (Exception e) {
      log("UID file error: " + e);
      return(null);
    }
***/
  }

// write uid, timestamp, message

  void log(String msg)
  {
    if (needFileInit){ // open file
      try{
        logWriter=new PrintWriter(new BufferedWriter(new FileWriter(
                  FILEPATH+programID+"/logs/" + userID + ".txt",true))); // open/append
      } catch (Exception e) {
        System.out.println("Logfile write error: " + e);
      }
      needFileInit=false;
    }

// ready to write
    System.out.println("["+theSocket.getPort() + "] " + LocalDateTime.now() + " " + msg);
    logWriter.println("["+theSocket.getPort() + "] " + LocalDateTime.now() + " " + msg);
  }

// save server for later use...
  void saveLogger(Logger log)
  {
    logger=log;
  }

}
