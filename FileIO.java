import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.FileWriter;

/*
 * Copyright 2018 Nicholas J. Macias
 * Licensed under the Creative Commons Share-alike - Attribution 4.0
 * License. You are free to use/modify/redistribute this work without charge,
 * provided you follow the license terms. See
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 * for more details.
 */

public class FileIO {
  static String read(String fname)
  {
    String ret="";
    try{
      BufferedReader br=new BufferedReader(new FileReader(fname));
      String temp;
      while ((temp=br.readLine())!=null){
        ret=ret+temp+"\n";
      }
      br.close();
    } catch (Exception e) {
      System.out.println("File read error: " + e);
    }
    return(ret);
  }

  static void write(String data, String fname)
  {
    try{
      PrintWriter pw=new PrintWriter(new BufferedWriter(new FileWriter(fname)));
      pw.print(data);
      pw.close();
    } catch (Exception e) {
      System.out.println("File write error: " + e);
    }
    return;
  }

}
